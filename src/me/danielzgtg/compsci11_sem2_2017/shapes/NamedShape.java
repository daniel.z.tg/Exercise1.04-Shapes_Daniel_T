package me.danielzgtg.compsci11_sem2_2017.shapes;

/**
 * A shape drawing function that has a human-friendly name.
 *
 * @author Daniel Tang
 * @since 24 February 2017
 */
public class NamedShape {

	/**
	 * The name of the shape.
	 */
	public final String name;

	/**
	 * The function used for drawing the shape.
	 */
	public final ShapeDrawer drawFunc;

	/**
	 * Creates a new named shape container.
	 *
	 * @param name The name of the shape.
	 * @param drawFunc The function used for drawing the shape.
	 */
	public NamedShape(final String name, final ShapeDrawer drawFunc) {
		this.name = name;
		this.drawFunc = drawFunc;
	}
}
