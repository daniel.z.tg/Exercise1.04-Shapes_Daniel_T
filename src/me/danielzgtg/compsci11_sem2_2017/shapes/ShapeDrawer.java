package me.danielzgtg.compsci11_sem2_2017.shapes;

/**
 * A interface to get method references to the shape drawing methods to work.
 *
 * @author Daniel Tang
 * @since 24 February 2017
 */
@FunctionalInterface
public interface ShapeDrawer {

	/**
	 * Draws a pattern.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	void draw(final int height, final char stamp);
}
