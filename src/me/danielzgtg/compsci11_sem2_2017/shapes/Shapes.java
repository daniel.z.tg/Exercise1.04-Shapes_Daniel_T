package me.danielzgtg.compsci11_sem2_2017.shapes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * A program to bring shapes to the console.
 *
 * @author Daniel Tang
 * @since 24 February 2017
 */
public final class Shapes {

	/**
	 * The introduction message displayed one at the beginning of the program.
	 */
	private static final String INTRODUCTION =
			"~ A Variety of Shapes ~";

	/**
	 * The header for the list of shapes.
	 */
	private static final String SHAPES_INTRODUCTION =
			"You can view any of the following shapes:";

	/**
	 * The list layout for the individual shape selections.
	 */
	private static final String SHAPES_MENU_LAYOUT =
			"%d %s\n";

	/**
	 * The prompt layout for obtaining the one-indexed index number of the shape selection intented to be displayed.
	 */
	private static final String SHAPE_PROMPT =
			"Enter an integer to choose a shape: ";

	/**
	 * The error message when the shape selection index could not be understood as an integer.
	 */
	private static final String ERR_NOT_INTEGER =
			"That input could not be understood as an integer, please try again.";

	/**
	 * The error message when the shape selection index does not correspond to a valid shape selection.
	 */
	private static final String ERR_SHAPE_LIST_OUTOF_BOUNDS =
			"That choice is not part of the list, please try again.";

	/**
	 * The prompt layout for the number of lines the shape should take.
	 */
	private static final String HEIGHT_PROMPT =
			"\nEnter the height of the shape: ";

	/**
	 * The warning message when the shape is large enough to potentially cause glitches.
	 */
	private static final String WARN_LARGE_HEIGHT_TEXT =
			"Warning: Large heights may cause glitches.";

	/**
	 * The height threshold for displaying the large height glitch warning message.
	 */
	private static final int WARN_LARGE_HEIGHT_THRESHOLD = 200;

	/**
	 * The prompt layout for obtaining a stamp character from the user.
	 */
	private static final String CHAR_PROMPT =
			"Enter a character: ";

	/**
	 * The error message when the pattern stamp was not just one character.
	 */
	private static final String ERR_NOT_ONE_CHAR =
			"The pattern can only be drawn by one character, please try again.";

	/**
	 * The prompt layout for whether to run the pattern selection and output again.
	 */
	private static final String CONTINUE_PROMPT = "Do you want to play again? (y/n): ";

	/**
	 * The error message when the user's intention for going again was not understood.
	 */
	private static final String CONTINUE_ERR_MSG = "Sorry! I didn't understand that choice, please try again.";

	/**
	 * The lowercase list of aliases that the user could choose to repeat with.
	 */
	private static final List<String> BOOLEAN_NAMES_TRUE = Collections.unmodifiableList(
			Arrays.asList("true", "yes", "t", "y", "yea", "woot"));

	/**
	 * The lowercase list of aliases that hte user could choose to not repeat with.
	 */
	private static final List<String> BOOLEAN_NAMES_FALSE = Collections.unmodifiableList(
			Arrays.asList("false", "no", "f", "n", "nay", "meh"));

	/**
	 * A list of named shape drawing methods.
	 */
	private static final List<NamedShape> SHAPES;

	public static void main(final String[] ignore) {
		System.out.println(INTRODUCTION); // Say Hi!

		try (final Scanner scanner = new Scanner(System.in)) {
			// Playing flag to support play again feature
			boolean playing = false;

			do {
				play(scanner); // Play the game
				playing = promptBoolean(scanner, CONTINUE_PROMPT, CONTINUE_ERR_MSG); // Ask if they want to play again
			} while (playing);
		}
	}

	/**
	 * The actual pattern generation and output.
	 *
	 * @param scanner The scanner for the console.
	 */
	@SuppressWarnings("SuspiciousNameCombination")
	private static void play(final Scanner scanner) {

		/*
		 * Obtain a selection index from the user,
		 * and store it in a variable that was declared and initialized to an invalid value.
		 * Then retrieve the drawing function from it.
		 */
		int selection = -1;
		while (true) {
			introduceShapes(); // Display an introduction

			System.out.print(SHAPE_PROMPT); // Prompt the user for the index of their selection of the list

			try {
				/*
				 * Store the obtained number
				 * Actual internal value is zero-indexed,
				 * but user value is one-indexed,
				 * so subtract one to convert
				 */
				selection = scanner.nextInt() - 1;
			} catch (final InputMismatchException ime) {
				System.out.println(ERR_NOT_INTEGER); // Something went wrong, so complain
				scanner.nextLine(); // Throw away garbage including the newline
				continue; // Try again
			}

			if (selection < 0 || selection >= SHAPES.size()) {
				System.out.println(ERR_SHAPE_LIST_OUTOF_BOUNDS); // Out of bounds, so complain
				scanner.nextLine(); // Throw away garbage including the newline
				continue; // Try again
			}

			scanner.nextLine(); // Throw away garbage including the newline
			break; // Yay, so we don't need to try again
		}

		// Retrieve the draw function
		final ShapeDrawer drawFunc = SHAPES.get(selection).drawFunc;

		/*
		 * Obtain the height from the user and store it in a variable
		 */
		int height;
		while (true) {
			System.out.print(HEIGHT_PROMPT); // Prompt the user for the height

			try {
				height = scanner.nextInt(); // Store the obtained number
			} catch (final InputMismatchException ime) {
				// Same as with selection index. See above.
				System.out.println(ERR_NOT_INTEGER);
				scanner.nextLine();
				continue;
			}

			scanner.nextLine(); // Throw away garbage including the newline
			break; // Yay, so we don't need to try again
		}

		if (height == 0) {
			/*
			 * When the height is zero,
			 * it doesn't matter which character is used to draw things.
			 * It will always be blank, so don't bother asking.
			 * We don't need to draw anything, so don't bother calling the draw function.
			 */
			return;
		}

		/*
		 * The user probably meant to enter a positive integer.
		 *
		 * Also, the only time you would have a negative height is when one is measuring in the opposite direction,
		 * in which the positive height is equivalent, so convert it to the positive one to loop properly.
		 */
		height = Math.abs(height);

		// Warn the user when the height is too large
		if (height >= WARN_LARGE_HEIGHT_THRESHOLD) {
			System.out.println(WARN_LARGE_HEIGHT_TEXT);
		}

		/*
		 * Obtain a stamp character and store it in a variable
		 */
		char stamp;
		while (true) {
			String tryInput; // Entire line the user inputs

			System.out.print(CHAR_PROMPT); // Prompt the user for the charcter
			tryInput = scanner.nextLine(); // Temporarily store the entire line

			// A stamp must be exactly 1 character in width, or else things will not look right, so check for it
			if (tryInput.length() != 1) {
				System.out.println(ERR_NOT_ONE_CHAR); // Complain to the user
				continue; // and try again
			}

			stamp = tryInput.charAt(0); // The String only contains one character, so coerce it to a char
			break; // Yay, so we don't need to try again
		}

		drawFunc.draw(height, stamp); // Actually draw the pattern
	}

	/**
	 * Displays an introduction of the available patterns.
	 */
	private static void introduceShapes() {
		System.out.println(SHAPES_INTRODUCTION); // Print list title


		// Print out a one-indexed numbered list of the names of the patterns
		for (int i = 0; i < SHAPES.size(); i++) {
			System.out.format(SHAPES_MENU_LAYOUT,
					i + 1, // 1, 2, 3, List numbers
					SHAPES.get(i).name // User-friendly name of the pattern to select
			);
		}
	}

	static {
		/*
		 * Name the methods and put them into a list
		 */
		final List<NamedShape> tmpShapes = new ArrayList<>();

		tmpShapes.add(new NamedShape("Square", Shapes::drawSquare));
		tmpShapes.add(new NamedShape("Right-angle Triangle", Shapes::drawTriangle));
		tmpShapes.add(new NamedShape("Pyramid", Shapes::drawPyramid));
		tmpShapes.add(new NamedShape("Hourglass", Shapes::drawHourglass));
		tmpShapes.add(new NamedShape("Diamond", Shapes::drawDiamond));
		tmpShapes.add(new NamedShape("Bowtie", Shapes::drawBowtie));

		SHAPES = Collections.unmodifiableList(tmpShapes); // Store the list
	}

	/**
	 * Draws a square pattern according the specified parameters.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	private static final void drawSquare(final int height, final char stamp) {
		// A square is simply a filled x and y grid.
		for (int x = 0; x < height; x++) {
			for (int y = 0; y < height; y++) {
				System.out.print(stamp); // Print the character at the position

				/*
				 * We need to insert a space in between the characters to style it correctly.
				 *
				 * But if there is no next column in this row,
				 * then it means we are at the end of the line,
				 * and a space at the end of the line is not visible to the user,
				 * so don't bother.
				 *
				 * So let's check for the next column, and only print if it exists.
				 */
				if ((y + 1) < height) {
					System.out.print(" ");
				}
			}

			System.out.println(); // Advance to the next row
		}
	}

	/**
	 * Draws a triangle pattern according the specified parameters.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	private static final void drawTriangle(final int height, final char stamp) {
		for (int x = 0; x < height; x++) { // Iterate and consider all rows
			/*
			 * In a regular right-angled isosceles triangle,
			 * there are row number number of columns.
			 *
			 * Row number starts at 0, we start at (0) 1 column,
			 * then goes to 1, and (0, 1) 2,
			 * then goes to n, and (0, 1, ..., n) n + 1
			 *
			 * n + 1 <= y converted to zero-indexed experssion:
			 * n + 1 - 1 <= y
			 * n <= y
			 */
			for (int y = 0; y <= x; y++) {
				System.out.print(stamp);

				// Similar to square, See above
				if (y < x) {
					System.out.print(' ');
				}
			}

			System.out.println(); // See above
		}
	}

	/**
	 * Draws a pyramid pattern according the specified parameters.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	private static final void drawPyramid(final int height, final char stamp) {
		for (int x = 0; x < height; x++) { // Iterate and consider all rows

			// Insert the space to the left of the pyramid
			// The math is magic. Don't think about it. It will mess up most people's heads. Don't worry about it.
			for (int i = (x + 1) * 2; i < (height * 2); i++) {
				System.out.print(" ");
			}

			// Draw the actual pyramid
			// Math is magic
			for (int y = 0; y < (x * 2 + 1); y++) {
				System.out.print(stamp);

				// Similar to square, See above
				if ((y + 1) < (x * 2 + 1)) {
					System.out.print(" ");
				}
			}

			System.out.println();
		}
	}

	/**
	 * Draws a hourglass pattern according the specified parameters.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	private static final void drawHourglass(final int height, final char stamp) {
		for (int x = 0; x < height; x++) {
			// A Hourglass is just two pyramids stuck to each other
			final boolean topHalf = x < (height / 2);

			// Insert the spaces. Math is magic.
			for (int i = 0; i < (topHalf ? (x * 2) : ((height - x) * 2) - 2); i++) {
				System.out.print(" ");
			}

			// Draw the actual hourglass. Math is magic.
			final int currentMax = topHalf ? height - x * 2 : x * 2 - height + 2;
			for (int y = 0; y < currentMax; y++) {
				System.out.print(stamp);

				// Similar to Square, see above.
				if ((y + 1) < currentMax) {
					System.out.print(" ");
				}
			}

			System.out.println();
		}
	}

	/**
	 * Draws a diamond pattern according the specified parameters.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	private static final void drawDiamond(final int height, final char stamp) {
		for (int x = 0; x < height; x++) {
			// A diamond is two pyramids stuck to each other the other way.
			final boolean topHalf = x < (height / 2);

			// Insert the spaces. Math is magic.
			for (int i = 0; i < (topHalf ? (height - 1) / 2 - x : x - (height / 2)); i++) {
				System.out.print(" ");
			}

			// Draw the actual diamond. Math is magic.
			final int currentMax = topHalf ? x + 1 : height - x;
			for (int y = 0; y < currentMax; y++) {
				System.out.print(stamp);

				// Similar to square, See above.
				if ((y + 1) < currentMax) {
					System.out.print(" ");
				}
			}

			System.out.println();
		}
	}

	/**
	 * Draws a bowtie pattern according the specified parameters.
	 *
	 * @param height The number of lines to use.
	 * @param stamp The character the pattern is drawn with.
	 */
	private static final void drawBowtie(final int height, final char stamp) {
		final int width = (height - 1) / 2 * 2 + 1;

		for (int x = 0; x < height; x++) {
			// A bowtie curves in first, then curves out
			final boolean topHalf = x < (height / 2);

			for (int y = 0; y < width; y++) {
				System.out.print(Math.min(y, width - y - 1) // Bowties curve in the middle, so do magic to mirror
						// Math is magic
						< (topHalf ? x + 1 : height - x) ? stamp : ' ');

				// Same as for square, See above.
				if ((y + 1) < width) {
					System.out.print(' ');
				}
			}

			System.out.println();
		}
	}

	/**
	 * Obtains a yes/no answer from the user.
	 *
	 * @param scanner The scanner to obtain input with.
	 * @param prompt The prompt message to present the user with.
	 * @param errorMsg The error message to show to the user if we did not understand.
	 * @return {@code true} if the user responded yes.
	 */
	private static final boolean promptBoolean(final Scanner scanner, final String prompt, final String errorMsg) {
		while (true) {
			System.out.print(prompt); // Present the prompt message.
			final String input = scanner.nextLine().toLowerCase(); // Obtain all input.

			if (BOOLEAN_NAMES_TRUE.contains(input)) { // true if it is thought as "yes"
				return true;
			} else if (BOOLEAN_NAMES_FALSE.contains(input)) { // false if it is thought as "no"
				return false;
			} else {
				// Show an error message, and try agian
				System.out.println(errorMsg);
			}
		}
	}

	private Shapes() { throw new UnsupportedOperationException(); }
}
